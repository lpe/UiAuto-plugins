import csv


def openCSV(params):
    try:
        path = params["path"]
        with open(path) as objFile:
            reader = csv.reader(objFile)
            return list(reader)
    # 文件操作失败时返回空数组，确保类型一致
        return []
    except Exception as e:
        raise e


def saveCSV(params):
    try:
        data = params["data"]
        path = params["path"]
        with open(path, "w", newline='') as objFile:
            writer = csv.writer(objFile)
            for row in data:
                writer.writerow(row)
    except Exception as e:
        raise e
