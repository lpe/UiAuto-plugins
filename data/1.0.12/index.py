import json
import math
import re
from time import localtime, time, strptime, mktime
from datetime import datetime, timedelta
import demjson
import win32con
import win32clipboard as wincld
import pandas as pd


def filterArray(params):
    try:
        array = params['array']
        ele = params["ele"]
        isSave = params["isSave"]
        aList = list()
        if ele in array:
            if isSave == 'yes':
                return array
            else:
                for a in array:
                    if a != ele:
                        aList.append(a)
                return aList
        return array
    except Exception as e:
        raise e


def toString(params):
    try:
        array = params['array']
        s = str(params["s"])
        array = map(str, array)
        string = s.join(array)
        return string
    except Exception as e:
        raise e


def getIndex(params):
    try:
        array = params['array']
        # s_list = array.split(",")
        if len(array) == 0:
            data = 0
        else:
            data = len(array) - 1
        return data
    except Exception as e:
        raise e


def create(params):
    try:

        objSet = list()
        return objSet
    except Exception as e:
        raise e


def add(params):
    try:
        # global objSet
        if isinstance(params['set'], list):
            s = set(params['set'])

        ele = params['ele']
        s.add(ele)
        result = list(s)

        return result
    except Exception as e:
        raise e


def remove(params):
    try:
        # global objSet
        if isinstance(params["set"], list):
            s = set(params["set"])

        ele = params['ele']

        s.remove(ele)
        result = list(s)

        return result
    except Exception as e:
        raise e


def get_size(params):
    try:
        # global objSet
        if isinstance(params['set'], list):
            objSet = set(params['set'])
        else:
            raise Exception("集合格式不正确")

        # s_set = set(s_list)
        length = len(objSet)
        return length
    except Exception as e:
        raise e


def getIntersection(params):
    try:
        # s = params["s"].replace("{", "").replace("}", "").replace("'", "")
        # s1 = params["s1"].replace("{", "").replace("}", "").replace("'", "")
        if isinstance(params['s'], list) and isinstance(params['s1'], list):
            s = set(params['s'])
            s1 = set(params['s1'])
        else:
            raise Exception("集合格式不正确")

        intersection = s.intersection(s1)
        result = ''
        # for e in intersection:

        if len(intersection) == 0:
            return "没有交集"
        return list(intersection)
    except Exception as e:
        raise e


def hasIntersection(params):
    try:
        if isinstance(params['s'], list) and isinstance(params['s1'], list):
            s = set(params['s'])
            s1 = set(params['s1'])
        else:
            raise Exception("集合格式不正确")

        intersection = s.intersection(s1)
        if len(intersection) == 0:
            return "没有交集"
        else:
            return list(intersection)
    except Exception as e:
        raise e


def isSubset(params):
    try:

        if isinstance(params['s'], list) and isinstance(params['s1'], list):
            s = set(params['s'])
            s1 = set(params['s1'])
        else:
            raise Exception("集合格式不正确")
        if s.issubset(s1) is False:
            return "不是子集"
        else:
            return "是子集"
    except Exception as e:
        raise e


def isSuperset(params):
    try:

        if isinstance(params['s'], list) and isinstance(params['s1'], list):
            s = set(params['s'])
            s1 = set(params['s1'])
        else:
            raise Exception("集合格式不正确")
        if s.issuperset(s1) is False:
            return "不是父集"
        else:
            return "是父集"
    except Exception as e:
        raise e


def getDiffer(params):
    try:
        if isinstance(params['s'], list) and isinstance(params['s1'], list):
            s = set(params['s'])
            s1 = set(params['s1'])
        else:
            raise Exception("集合格式不正确")
        differ = s - s1
        if len(differ) == 0:
            return "没有差集"
        return list(differ)
        # result = ''
    except Exception as e:
        raise e


def union(params):
    try:

        if isinstance(params['s'], list) and isinstance(params['s1'], list):
            s = set(params['s'])
            s1 = set(params['s1'])
        else:
            raise Exception("集合格式不正确")
        u_set = s.union(s1)
        if len(u_set) == 0:
            return "没有并集"
        return list(u_set)
        # result = ''

    except Exception as e:
        raise e


def setToArray(params):
    try:
        if isinstance(params['set'], list):
            s = params['set']
        else:
            raise Exception("集合格式不正确")
        return s

    except Exception as e:
        raise e


def strToJson(params):
    try:
        string = params['string']

        jsonstr = json.dumps(string)
        return jsonstr
    except Exception as e:
        raise e


def JsonToStr(params):
    try:
        string = params['string']

        strjson = json.loads(string)
        return str(strjson)
    except Exception as e:
        raise e


def fabs(params):
    try:
        data = math.fabs(params['num_f'])
        return data
    except Exception as e:
        raise e


def sqrt(params):
    try:
        if params['num_s'] < 0:
            raise Exception("输入的数小于0")
        else:
            return math.sqrt(params['num_s'])
    except Exception as e:
        raise e


def floor(params):
    try:
        return math.floor(params['num_fl'])
    except Exception as e:
        raise e


def round_data(params):
    from decimal import Decimal
    try:
        num = str(params['num_r'])
        n = params['n']
        if n != '' and n != 0:
            index = num.index('.')
            new_num = num[:index] + num[index:n+3]
            if new_num[-1] == '5' and len(new_num[index+1:]) != n:
                num_list = list(new_num)
                num_list[-1] = '6'
                new_num = ''.join(num_list)
            data = round(float(new_num), n)
            print("有小数",data)
        elif n == 0:
            data = int(round(float(num)))
            print("没小数",data)

        return data
    except Exception as e:
        raise e


def factorial(params):

    try:
        return math.factorial(params['num'])
    except ValueError:
        raise Exception("输入的数字不正确")
    except Exception as e:
        raise e


def log(params):
    try:
        return math.log(params['num'])
    except ValueError:
        raise Exception("输入的数字不正确")
    except Exception as e:
        raise e


def cos(params):
    try:
        return math.cos(params['num'])
    except Exception as e:
        raise e


def sin(params):
    try:
        return math.sin(params['num'])
    except Exception as e:
        raise e


def tan(params):
    try:
        return math.tan(params['num'])
    except Exception as e:
        raise e


def atan(params):
    try:
        return math.atan(params['num'])
    except Exception as e:
        raise e


def exp(params):
    try:
        return math.exp(params['num'])
    except Exception as e:
        raise e


def find(params):
    try:
        # global pDict
        string = params['string']
        regex = params['regex']
        # p = params['p']
        if params['p'] != '' and params['p']:
            pDict = params['p']
            if isinstance(pDict, dict):
                # p = params['p']
                U, S, I, L, M, X = 0, 0, 0, 0, 0, 0
                if 'line' in pDict.keys() and pDict['line'] == 'yes':
                    # line = p['line']
                    # if pDict['line'] == 'yes':
                    S = re.S
                if 'case' in pDict.keys() and pDict['case'] == 'yes':
                    # case = p['case']
                    # if pDict['case'] == 'yes':
                    I = re.I
                if "locale" in pDict.keys() and pDict['locale'] == 'yes':
                    # locale = p['locale']
                    # if pDict['locale'] == 'yes':
                    L = re.L
                if 'more' in pDict.keys() and pDict['more'] == 'yes':
                    # more = p['more']
                    # if pDict['more'] == 'yes':
                    M = re.M
                if 'sweet' in pDict.keys() and pDict['sweet'] == 'yes':
                    # sweet = p['sweet']
                    # if pDict['sweet'] == 'yes':
                    X = re.X
                if 'unicode' in pDict.keys() and pDict['unicode'] == 'yes':
                    # unicode = p['unicode']
                    # if pDict['unicode'] == 'yes':
                    U = re.U
                flags = (U | X | M | L | I | S)
        else:
            flags = 0

        objPattern = re.compile(regex, flags)
        objMatcher = re.search(objPattern, string)
        if objMatcher is None:
            return []
        else:
            arrRet = []
            arrRet.append(objMatcher.group(0))
            for s in objMatcher.groups():
                arrRet.append(s)
            return arrRet
        # return pDict
    except Exception as e:
        raise e


def findChild(params):
    try:
        # global pDict
        string = params['string']
        regex = params['regex']
        num = int(params["num"])
        if params['p'] and params['p'] != '':
            p = params['p']
            U, S, I, L, M, X = 0, 0, 0, 0, 0, 0
            if 'line' in p.keys():
                line = p['line']
                if line == 'yes':
                    S = re.S
            if 'case' in p.keys():
                case = p['case']
                if case == 'yes':
                    I = re.I
            if "locale" in p.keys():
                locale = p['locale']
                if locale == 'yes':
                    L = re.L
            if 'more' in p.keys():
                more = p['more']
                if more == 'yes':
                    M = re.M
            if 'sweet' in p.keys():
                sweet = p['sweet']
                if sweet == 'yes':
                    X = re.X
            if 'unicode' in p.keys():
                unicode = p['unicode']
                if unicode == 'yes':
                    U = re.U
            flags = (U | X | M | L | I | S)
        else:
            flags = 0

        print(flags)
        print(params['p'])

        objPattern = re.compile(regex, flags)
        objMatcher = re.search(objPattern, string)
        if objMatcher is None:
            return ""
        else:
            return objMatcher.group(num)


    except Exception as e:
        raise e


def findAll(params):
    try:
        # global pDict
        string = params['string']
        regex = params['regex']
        if params['p'] and params['p'] != '':
            p = params['p']
            U, S, I, L, M, X = 0, 0, 0, 0, 0, 0
            if 'line' in p.keys():
                line = p['line']
                if line == 'yes':
                    S = re.S
            if 'case' in p.keys():
                case = p['case']
                if case == 'yes':
                    I = re.I
            if "locale" in p.keys():
                locale = p['locale']
                if locale == 'yes':
                    L = re.L
            if 'more' in p.keys():
                more = p['more']
                if more == 'yes':
                    M = re.M
            if 'sweet' in p.keys():
                sweet = p['sweet']
                if sweet == 'yes':
                    X = re.X
            if 'unicode' in p.keys():
                unicode = p['unicode']
                if unicode == 'yes':
                    U = re.U
            flags = (U | X | M | L | I | S)
        else:
            flags = 0
        pattern = re.compile(regex, flags)
        it = pattern.findall(string)
        # result = '['
        # if len(it) > 0:

        #     return it
        # else:
        #     return []
        return it
    except Exception as e:
        raise e


def setMatchLine(params):
    try:
        # global pDict
        # line = params['line']
        if params['p'] != '' and params['p'] != '${}':
            p = params['p']
        else:
            p = dict()
        # if type(p) != dict:
        #     pDict = dict()
        if params['line'] == 'yes':
            p['line'] = 'yes'
        else:
            p['line'] = 'no'
        return p
    except Exception as e:
        raise e


def setIgnoreCase(params):
    try:
        # global pDict
        # case = params['case']
        if params['p'] != '' and params['p'] != '${}':
            p = params['p']
        else:
            p = dict()
        # if not isinstance(pDict, dict):
        #     pDict = dict()
        if params['case'] == 'yes':
            p['case'] = 'yes'
        else:
            p['case'] = 'no'
        return p
    except Exception as e:
        raise e


def setLocale(params):
    try:
        # locale = params['locale']
        # p = dict()
        if params['p'] != '' and params['p']:
            p = params['p']
        else:
            p = dict()
        if params['locale'] == 'yes':
            p['locale'] = 'yes'
        else:
            p['locale'] = 'no'
        return p
    except Exception as e:
        raise e


def setMatchMoreLines(params):
    try:
        more = params['more']
        if params['p'] != '' and params['p']:
            p = params['p']
        else:
            p = dict()
        if params['more'] == 'yes':
            p['more'] = 'yes'
        else:
            p['more'] = 'no'
        return p
    except Exception as e:
        raise e


def setSweet(params):
    try:
        sweet = params['sweet']
        if params['p'] != '' and params['p']:
            p = params['p']
        else:
            p = dict()
        if sweet == 'yes':
            p['sweet'] = 'yes'
        else:
            p['sweet'] = 'no'
        return p
    except Exception as e:
        raise e


def setUnicode(params):
    try:
        unicode = params['unicode']
        if params['p'] != '' and params['p']:
            p = params['p']
        else:
            p = dict()
        if unicode == 'yes':
            p['unicode'] = 'yes'
        else:
            p['unicode'] = 'no'
        return p
    except Exception as e:
        raise e


def get_time(params):
    try:
        time_str = time()
        return time_str
    except Exception as e:
        raise e


def get_year(params):
    try:
        year = localtime(get_time(params)).tm_year
        return year
    except Exception as e:
        raise e


def get_month(params):
    try:
        month = localtime(get_time(params)).tm_mon
        return month
    except Exception as e:
        raise e


def get_day(params):
    try:
        day = localtime(get_time(params)).tm_mday
        return day
    except Exception as e:
        raise e


def get_hour(params):
    try:
        hour = localtime(get_time(params)).tm_hour
        return hour
    except Exception as e:
        raise e


def get_minute(params):
    try:
        minute = localtime(get_time(params)).tm_min
        return minute
    except Exception as e:
        raise e


def get_second(params):
    try:
        second = localtime(get_time(params)).tm_sec
        return second
    except Exception as e:
        raise e


def get_weekday(params):
    try:
        weekday = localtime(get_time(params)).tm_wday
        return weekday + 1
    except Exception as e:
        raise e


def get_week(params):
    try:
        num_day = ['一', '二', '三', '四', '五', '六', '日']
        week = num_day[get_weekday(params) - 1]
        return '星期' + week
    except Exception as e:
        raise e


def get_some(params):
    try:
        if 'year' == str(params['str']):
            data = get_year(params)
        elif 'month' == str(params['str']):
            data = get_month(params)
        elif 'day' == str(params['str']):
            data = get_day(params)
        elif 'hour' == str(params['str']):
            data = get_hour(params)
        elif 'minute' == str(params['str']):
            data = get_minute(params)
        elif 'second' == str(params['str']):
            data = get_second(params)
        elif 'week' == str(params['str']):
            data = get_week(params)

        return data
    except Exception as e:
        raise e


def cal_time(params):
    try:
        date1 = str(params['date1'])
        date2 = str(params['date2'])

        timeArray1 = strptime(date1, "%Y-%m-%d %H:%M:%S")
        timeArray2 = strptime(date2, "%Y-%m-%d %H:%M:%S")

        timestamp1 = mktime(timeArray1)
        timestamp2 = mktime(timeArray2)

        caltime = int(timestamp2 - timestamp1)

        if 'year' == str(params['str']):
            data = caltime / 60 / 60 / 24 / 365
        elif 'month' == str(params['str']):
            data = caltime / 60 / 60 / 24 / 30
        elif 'quarter' == str(params['str']):
            data = caltime / 60 / 60 / 24 / 30 / 3
        elif 'day' == str(params['str']):
            data = caltime / 60 / 60 / 24
        elif 'week' == str(params['str']):
            data = caltime / 60 / 60 / 24 / 7
        elif 'hour' == str(params['str']):
            data = caltime / 60 / 60
        elif 'minute' == str(params['str']):
            data = caltime / 60
        elif 'second' == str(params['str']):
            data = caltime
        if data < 1 and data > 0:
            data = 1
        if data == 0.0:
            data = 0
        return int(data)
    except Exception as e:
        raise e


def addTime(params):
    try:
        timeStr = str(params['time'])
        num = params['num']
        unit = params['unit']
        addStr = str(params['add'])
        addDic = demjson.decode(addStr)
        if num > 0:
            if unit in addDic.keys():
                addDic[unit] = addDic[unit] + num
            else:
                addDic[unit] = num
        # addDic = json.loads(addStr)
        time = datetime.strptime(timeStr, "%Y-%m-%d %H:%M:%S")
        for key, value in addDic.items():
            if key == 'years' or key == "y":
                time = timedelta(days=365 * value) + time
            if key == 'quarters' or key == "Q":
                time = timedelta(days=90 * value) + time
            if key == 'months' or key == "M":
                time = timedelta(days=30 * value) + time
            if key == 'weeks' or key == "w":
                time = timedelta(weeks=1 * value) + time
            if key == 'days' or key == "d":
                time = timedelta(days=1 * value) + time
            if key == 'hours' or key == "h":
                time = timedelta(hours=1 * value) + time
            if key == 'minutes' or key == "m":
                time = timedelta(minutes=1 * value) + time
            if key == 'seconds' or key == "s":
                time = timedelta(seconds=1 * value) + time
            if key == 'milliseconds' or key == "ms":
                time = timedelta(milliseconds=1 * value) + time
        return time.strftime("%Y-%m-%d %H:%M:%S")
    except ValueError as e:
        return e.args[0]
    except Exception as e:
        raise e


def subTime(params):
    try:
        timeStr = str(params['time'])
        num = params['num']
        unit = params['unit']
        addDic = dict()
        if str(params["sub"]).replace(" ", "").replace("\n", "") != "{}":
            addStr = str(params['sub'])
            addDic = demjson.decode(addStr)
        if num > 0:
            if addDic is not None and unit in addDic.keys():
                addDic[unit] = addDic[unit] + num
            else:
                addDic[unit] = num
        # addDic = json.loads(addStr)
        time = datetime.strptime(timeStr, "%Y-%m-%d %H:%M:%S")
        for key, value in addDic.items():
            if key == 'years' or key == "y":
                time = time - timedelta(days=365 * value)
            if key == 'quarters' or key == "Q":
                time = time - timedelta(days=90 * value)
            if key == 'months' or key == "M":
                time = time - timedelta(days=30 * value)
            if key == 'weeks' or key == "w":
                time = time - timedelta(weeks=1 * value)
            if key == 'days' or key == "d":
                time = time - timedelta(days=1 * value)
            if key == 'hours' or key == "h":
                time = time - timedelta(hours=1 * value)
            if key == 'minutes' or key == "m":
                time = time - timedelta(minutes=1 * value)
            if key == 'seconds' or key == "s":
                time = time - timedelta(seconds=1 * value)
            if key == 'milliseconds' or key == "ms":
                time = time - timedelta(milliseconds=1 * value)
        return time.strftime("%Y-%m-%d %H:%M:%S")
    except ValueError as e:
        return e.args[0]
    except Exception as e:
        raise e


def createDataTable(params):
    try:
        th = params['th']
        array = params['array']

        data = pd.DataFrame(array, columns=th)
        # data.fillna(value=None)

        # for col in th:
        #     clean_z = data[col].fillna(0)
        #     clean_z[clean_z==0.0] = "null"
        #     data[col] = clean_z

        string = data.to_dict()
        # toIo(string)

        return string
    except Exception as e:
        raise e


def toDataFrame(string):
    from io import StringIO
    TESTDATA = StringIO(string)
    df = pd.read_csv(TESTDATA, sep=";")
    return df


def toPaste(params):
    try:
        data = params["dt"]
        # df = toDataFrame(data)
        df = pd.DataFrame(data)
        df.to_clipboard()
        # set_text(data)
        return df
    except Exception as e:
        raise e


def dataFilter(params):
    try:
        condition = params['filter']
        colName = params['colName']
        value = params["c"]
        dt = params["dt"]

        df = pd.DataFrame(dt)
        # datatable = p['result']

        query = None
        if condition == 'bigger':
            if isinstance(value, int) or isinstance(value, float):
                query = colName + ">" + str(value)
            else:
                query = colName + ">\'" + str(value) + "\'"

        if condition == "smaller":
            # query = colName + "<" + value
            if isinstance(value, int) or isinstance(value, float):
                query = colName + "<" + str(value)
            else:
                query = colName + "<\'" + str(value) + "\'"
        if condition == "bigequal":
            # query = colName + ">=" + value
            if isinstance(value, int) or isinstance(value, float):
                query = colName + ">=" + str(value)
            else:
                query = colName + ">=\'" + str(value) + "\'"
        if condition == "smallequal":
            # query = colName + "<=" + value
            if isinstance(value, int) or isinstance(value, float):
                query = colName + "<=" + str(value)
            else:
                query = colName + "<=\'" + str(value) + "\'"
        if condition == "equal":
            # query = colName + "==" + value
            if isinstance(value, int) or isinstance(value, float):
                query = colName + "==" + str(value)
            else:
                query = colName + "==\'" + str(value) + "\'"
        if condition == "noequal":
            # query = colName + "!=" + value
            if isinstance(value, int) or isinstance(value, float):
                query = colName + "!=" + str(value)
            else:
                query = colName + "!=\'" + str(value) + "\'"
        if condition == "none":
            query = colName + "==" + "\'\'"
        if condition == "notnone":
            query = colName + "!=" + "\'\'"
        if condition == "start":
            query = colName + ".str.startswith(\'" + value + "\')"
        if condition == "end":
            query = colName + ".str.endswith(\'" + value + "\')"
        if condition == "nostart":
            query = "~" + colName + ".str.startswith(\'" + value + "\')"
        if condition == "noend":
            query = "~" + colName + ".str.endswith(\'" + value + "\')"
        if condition == "contain":
            query = colName + ".str.contains(\'" + value + "\')"
        if condition == "nocontain":
            query = "~" + colName + ".str.contains(\'" + value + "\')"
        print("条件：", query)
        result = df.query(query)
        return result
    except Exception as e:
        raise e


def dataCut(params):
    try:
        
        row = params['row']

        col = params['col']
        dt = params['dt']
        # dt = re.sub(r"\s{2,}", " ", dt)
        # print(dt)
        # df = toDataFrame(dt)
        df = pd.DataFrame(dt)

        length = len(row)
        if length == 0:
            result = df.loc[:, col]
        else:
            result = df.loc[row[0]:row[1], col]

        return result.to_string()

    except Exception as e:
        raise e


def dataSelect(params):
    try:

        col = params['col']
        data = params['dt']
        df = pd.DataFrame(data)
        array = df.filter(items=col)

        return array
    except Exception as e:
        raise e


def dataRepeat(params):
    try:

        col = params['col']
        data = params['dt']
        df = pd.DataFrame(data)
        keep = params['keep']
        if col == []:
            col = None
        result = df.drop_duplicates(subset=col, keep=keep)
        return result

    except Exception as e:
        raise e


def toArray(params):
    try:
        data = params['dt']
        hasCol = params['hasCol']
        df = pd.DataFrame(data)
        result = df.values.tolist()
        if hasCol == 'yes':
            cols = df.columns.tolist()
            result.insert(0, cols)

        return result
    except Exception as e:
        raise e


def tableCompare(params):
    try:
        data1 = params['dt']
        data2 = params['dt1']
        df = pd.DataFrame(data1)
        df1 = pd.DataFrame(data2)
        return df.equals(df1)
        # th1 = p1['col']

    except Exception as e:
        raise e


def addCol(params):
    try:
        data = params['dt']
        colName = params['colName']
        value = params['data']
        position = params['position']
        df = pd.DataFrame(data)
        if position == 'null':
            df[colName] = value
        else:
            position = int(position)
            df.insert(position, colName, value)
        return df
    except KeyError as e:
        raise e
    except Exception as e:
        raise e


def changeType(params):
    try:
        data = params['dt']
        col = params['col']
        dataType = params['type']
        # isExcept = params['except']
        value = params['data']
        df = pd.DataFrame(data)


        if type(col) == list:
            for c in col:
                df[c] = df[c].apply(convert, args=(dataType, value))
        else:
            df[col] = df[col].apply(convert, args=(dataType, value))
        return df
    except Exception as e:
        raise e


def convert(a, dataType, value):
    try:
        if dataType == 'int':
            return int(a)
        if dataType == 'float':
            return float(a)
        if dataType == 'str':
            return str(a)
    except ValueError as e:
        try:
            # if isExcept:
            #     raise e
            # else:
            return value
        finally:
            del e


def dataSort(params):
    try:
        sort = params['sort']
        data = params['dt']
        col = params['col']
        df = pd.DataFrame(data)
        if sort == 'yes':
            sort = True
        else:
            sort = False
        result = df.sort_values(by=col, ascending=sort)
        return result
    except Exception as e:
        raise e


def getColNum(params):
    try:
        data = params['dt']
        df = pd.DataFrame(data)
        return df.shape

    except Exception as e:
        raise e


def getColName(params):
    try:
        data = params['dt']
        df = pd.DataFrame(data)
        return df.columns.tolist()
    except Exception as e:
        raise e


def updateColName(params):
    try:
        data = params['dt']
        col = params['col']
        df = pd.DataFrame(data)
        df.columns = col
        return df

    except Exception as e:
        raise e


def tableMerge(params):
    try:
        sort = params['sort']
        data = params['dt']
        data1 = params['dt1']
        connect = params['connect']
        leftCol = params['leftCol']
        rightCol = params['rightCol']
        if sort == 'no':
            sort = False
        else:
            sort = True
        df = pd.DataFrame(data)
        df1 = pd.DataFrame(data1)
        result = pd.merge(df, df1, how=connect, left_on=leftCol,
                          right_on=rightCol, sort=sort)
        return result

    except Exception as e:
        raise e


def replaceStr(params):
    try:
        string = str(params['string'])
        target = str(params['target'])
        replace = str(params['repl'])
        ignore = params['ignore']
        newStr = string.replace(target, replace)
        if ignore == 'no':
            reg = re.compile(re.escape(target), re.IGNORECASE)
            newStr = reg.sub(replace, string)

        return newStr
    except Exception as e:
        raise e


def findStr(params):
    try:
        string = str(params['string'])
        target = str(params['target'])
        ignore = str(params["ignore"])

        position = int(params["pos"])
        if position == 0:
            raise Exception("请输入1或比1大的整数")
        if position >= 1:
            position = position - 1
        index = string.find(target, position)
        if ignore == 'no':
            tarList = re.findall(target, string, flags=re.IGNORECASE)
            if len(tarList) > 0:
                index = string.find(tarList[0], position)
            else:
                index = -1
        if index == -1:
            return 0

        return index + 1
    except Exception as e:
        raise e


def reFindStr(params):
    try:
        string = str(params['string'])
        target = str(params['target'])
        ignore = str(params["ignore"])
        try:
            position = int(params["pos"])
            if position == 0:
                return "请输入1或比1大的整数"
            if position >= 1:
                position = position - 1
        except ValueError:
            return "请输入数字"
        index = string.rfind(target, position)
        if ignore == 'no':
            tarList = re.findall(target, string, flags=re.IGNORECASE)
            index = string.find(tarList[len(tarList) - 1], position)
        if index == -1:
            return "没找到"

        return index + 1
    except Exception as e:
        raise e


def leftSub(params):
    try:
        string = str(params["string"])
        num = int(params['num'])
        result = string[:num]
        return result
    except Exception as e:
        raise e


def rightSub(params):
    try:
        string = str(params["string"])
        num = int(params['num'])
        result = string[-num:]
        return result
    except Exception as e:
        raise e


def middleSub(params):
    try:
        string = str(params["string"])
        num = int(params["num"]) - 1
        length = int(params["length"])
        result = string[num:length + num]
        return result
    except Exception as e:
        raise e


def getLength(params):
    try:
        string = params["string"]
        return len(string)
    except Exception as e:
        raise e


def getByteLength(params):
    try:
        string = str(params["string"])
        return len(string.encode())
    except Exception as e:
        raise e


def toUpper(params):
    try:
        string = params["string"]
        return string.upper()
    except Exception as e:
        raise e


def toLower(params):
    try:
        string = params["string"]
        return string.lower()
    except Exception as e:
        raise e


def getAscii(params):
    try:
        a = params["char"][0]
        return ord(a)
    except Exception as e:
        raise e


def toChar(params):
    try:
        code = int(params['code'])
        return chr(code)
    except Exception as e:
        raise e


def leftCut(params):
    try:
        string = str(params["string"])
        cut = str(params['cut'])
        while string.startswith(cut):
            string = string.lstrip(cut)
        return string
    except Exception as e:
        raise e


def rightCut(params):
    try:
        string = str(params["string"])
        cut = str(params['cut'])
        while string.endswith(cut):
            string = string.rstrip(cut)
        return string
    except Exception as e:
        raise e


def allCut(params):
    try:
        string = str(params["string"])
        cut = str(params['cut'])
        if string.startswith(cut):
            string = leftCut(params)
            params['string'] = string
        if string.endswith(cut):
            string = rightCut(params)
        return string
    except Exception as e:
        raise e


def getLenStr(params):
    try:
        string = str(params["string"])
        num = int(params["num"]) - 1
        length = int(params["length"])
        result = string[num:length + num]
        return result
    except Exception as e:
        raise e


def getPosStr(params):
    try:
        string = str(params["string"])
        num = int(params["start"]) - 1
        length = int(params["end"])
        result = string[num:length + num - 1]
        return result
    except Exception as e:
        raise e


def middleCut(params):
    try:
        string = str(params["string"])
        start = int(params['start'])
        length = int(params["length"])

        result = string[:start - 1] + string[length + start - 1:]

        return result
    except Exception as e:
        raise e


def createBlank(params):
    try:
        num = int(params["num"])
        return '"%s"' % (" " * num)
    except ValueError:
        raise Exception("请输入整数")
    except Exception as e:
        raise e


def createStr(params):
    try:
        num = int(params["num"])
        string = str(params["string"])
        return num * string
    except Exception as e:
        raise e


def splitStr(params):
    try:
        if str(params['s']) != "":
            s = str(params["s"])
        else:
            s = " "
        string = str(params["string"])
        slist = string.split(s)
        slist = list(filter(not_empty, slist))
        # result = print_result(slist)
        return slist
    except Exception as e:
        raise e


def compareStr(params):
    try:
        case = str(params["case"])
        string1 = str(params['string1'])
        string2 = str(params['string2'])
        if case == 'yes':
            reg = re.compile(re.escape(string1), re.IGNORECASE)
        else:
            reg = re.compile(re.escape(string1))
        if reg.match(string2):
            result = "相同"
        else:
            result = "不相同"
        return result
    except Exception as e:
        raise e


def compareLenStr(params):
    try:
        case = str(params["case"])
        string1 = str(params['string1'])
        string2 = str(params['string2'])
        length = int(params["length"])
        if length > 0:
            string1 = string1[:length]
            string2 = string2[:length]
        else:
            return "请输入正数"
        if case == 'yes':
            reg = re.compile(re.escape(string1), re.IGNORECASE)
        else:
            reg = re.compile(re.escape(string1))
        if reg.match(string2):
            result = "相同"
        else:
            result = "不相同"
        return result
    except Exception as e:
        raise e


def getChar(params):
    try:
        string = str(params["string"])
        position = int(params['pos'])
        result = string[position - 1:position]
        return result
    except Exception as e:
        raise e


def not_empty(s):
    return s and s.strip() and s is not None and s != ''


def reverseStr(params):
    try:
        string = params["string"]
        slist = list(string)
        slist.reverse()
        result = "".join(slist)
        return result
    except Exception as e:
        raise e


if __name__ == '__main__':
    pass
