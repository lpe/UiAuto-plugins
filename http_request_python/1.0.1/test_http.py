import requests

if __name__ == "__main__":
    header = {
        "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "zh-Hans-CN, zh-Hans; q=0.5",
        "Cache-Control": "no-cache",
        "Connection": "Keep-Alive",
        "Content-Length": "113",
        "Content-Type": "application/x-www-form-urlencoded",
        "Cookie": 'JSESSIONID=060673494E4F57A6A841B3D5803F5C76;rememberName=yes;rememberPass="";tdhfydm=440104;tdhyhid=liangjiaj;tdhyhkl=""',
        "Host": "146.0.250.209:82",
        "Referer": "http://146.0.250.209:82/zxxt/webapp/court/ajgl/myaj_zx_js.jsp",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko",
        "X-Requested-With": "XMLHttpRequest"
    }

    data = {
        "ajdz": "",
        "cj": "",
        "csx": "",
        "cxdj": "base",
        "dsr": "",
        "jarq1": "",
        "jarq2": "",
        "limit": "100",
        "myjs": "AJCBR",
        "r_qb": "0",
        "r_wj": "1",
        "r_yj": "0",
        "start": "0",
        "xh": "28459"
    }

    res = requests.post("http://146.0.250.209:82/zxxt/webapp/court/ajgl/loadgrid.do", headers=header, data=data)
    print(res.status_code == 200)
    print(res.text)
    print('\n')
    print(res.content)
    print('\n')
    print(dir(res))
